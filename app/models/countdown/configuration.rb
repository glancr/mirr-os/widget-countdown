# frozen_string_literal: true

module Countdown
  class Configuration < WidgetInstanceConfiguration
    attribute :count_to, :string, default: ""

    validates :count_to,
              format: { with: /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}/, message: 'Must be a valid timestamp' },
              allow_blank: true,
              allow_nil: true
  end
end
