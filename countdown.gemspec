require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'countdown/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name = 'countdown'
  s.version = Countdown::VERSION
  s.authors = ['Tobias Grasse']
  s.email = ['tg@glancr.de']
  s.homepage = 'https://glancr.de/modules/countdown'
  s.summary = 'mirr.OS widget that shows a countdown.'
  s.description = 'Shows a count-down to a given date.'
  s.license = 'MIT'
  s.metadata = { 'json' =>
                   {
                     type: 'widgets',
                     title: {
                       enGb: 'Count-down',
                       deDe: 'Countdown',
                       frFr: 'compte à rebours',
                       esEs: 'cuenta regresiva',
                       plPl: 'odliczanie',
                       koKr: '카운트 다운'
                     },
                     description: {
                       enGb: s.description,
                       deDe: 'Zeigt einen Countdown bis zu einem vorgegebenen Datum.',
                       frFr: 'Affiche un compte à rebours à une date donnée.',
                       esEs: 'Muestra una cuenta atrás para una fecha determinada.',
                       plPl: 'Pokazuje odliczanie do podanej daty.',
                       koKr: '지정된 날짜까지 카운트 다운을 표시합니다.'
                     },
                     sizes: [
                       {
                         w: 4,
                         h: 2
                       },
                       {
                         w: 6,
                         h: 4
                       }
                     ],
                     languages: %i[enGb deDe frFr esEs plPl koKr],
                     group: nil
                   }.to_json
  }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_development_dependency 'rails'
end
