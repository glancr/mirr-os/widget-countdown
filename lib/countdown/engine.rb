module Countdown
  class Engine < ::Rails::Engine
    isolate_namespace Countdown
    config.generators.api_only = true
  end
end
